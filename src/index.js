import React from "react";
import ReactDOM from "react-dom/client";
import { Homepage } from "./pages/Homepage";
import { App } from "./routes/App";
import { BrowserRouter } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
    <App/>
    </BrowserRouter>
  </React.StrictMode>
);