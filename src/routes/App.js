import React from "react";
import { Route, Routes } from "react-router-dom";
import { Homepage } from "../pages/Homepage";
import ButtonAppBar from "../assets/components/ButtonAppBar";

export const App = () => {
  return (
    <div>
      <ButtonAppBar />
      <Routes>
        <Route path="/" element={<Homepage />} />
      </Routes>
    </div>
  );
};
