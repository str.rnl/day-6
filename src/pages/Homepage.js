import { Box, Container, Grid, Paper } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import BasicCard from "../assets/components/BasicCard";

export const Homepage = () => {
  const [Data, setData] = useState([]);

  const getData = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: "http://localhost:3004/postgenerated",
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        console.log(response.data);
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const renderData = () => {
    if (Data) {
      const data = Data ? Data : [];
      return data.map((val) => {
        return (
          <Grid item md={6} sm={6} xs={12}>
            <BasicCard
              title={val.title}
              subheader={val.subheader}
              img={val.img}
              author={val.author}
              lastname={val.lastName}
              description={val.description}
            ></BasicCard>
          </Grid>
        );
      });
    }
  };

  return (
    <Container width={"1200px"} sx={{ mt: "30px" }}>
      <Grid container spacing={2}>
        <Grid item md={9} sm={12} xs={12}>
          <Grid container spacing={2}>
            {renderData()}
          </Grid>
        </Grid>

        <Grid item md={3}>
          <Box
            borderRadius={"6px"}
            border={"groove 1px"}
            padding={"10px"}
            // paddingLeft={"80px"}
            justifyContent={"center"}
            display={{ md: "block", sm: "none", xs: "none" }}
            fontFamily={"montserrat"}
          >
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
            <h1>side bar</h1>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};
