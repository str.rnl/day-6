import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function BasicCard(props) {
  return (
    <Card sx={{ maxWidth: "100%", fontFamily:"montserrat" }} >
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            R
          </Avatar>
        }
        title={props?.title}
        subheader={props?.subheader}
      />
      <CardMedia
        component="img"
        height="194"
        image={props?.img}
        alt="foto"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          Author : {props?.author} <br/>
          Last name : {props?.lastname}
        </Typography>
      </CardContent>
      <CardContent>
        <Typography variant="body2" color="text.secondary" fontFamily={"montserrat"}>
          {props?.description}
        </Typography>
      </CardContent>
    </Card>
  );
}